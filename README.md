# iac-lambda-start-stop-aws

Código terraform para start/stop de instancias EC2 na AWS.

## Modo de uso

Como base, foi usado o Ubuntu 20.04
- [ ] Atualize o sistema
- [ ] Instale e configure o aws cli (aws configure)
- [ ] Instale o Terraform
- [ ] Faça o Clone do projeto


```bash
# Atualizando o sistema
sudo apt update

# Instalando o aws cli
sudo apt install awscli
aws configure

# instalando o terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform

# Clonando o repo
git clone https://gitlab.com/soaresnetoh/iac-lambda-start-stop-aws.git && cd iac-lambda-start-stop-aws

```


># Como funciona na Console da AWS

Primeiramente deve-se criar um Politica no IAM para que o Lambda possa acessar EC2 e CloudWatch Logs:

### Policy (start_stop_ec2_instances) - EC2 - (StartInstances,StopInstances,DescribeInstances) (add - CloudWatch Logs)
### Role - Lambda - start_stop_ec2_instances - start_stop_ec2_instances_role

Depois 

https://boto3.amazonaws.com/v1/documentation/api/latest/guide/migrationec2.html  

Criar Lambda - Python 3.6 - Role start_stop_ec2_instances_role - codigo:

```python
# Function Name: schedule_ec2_instances
# Filtra instancias pelas tag de Name:Type e Values:Scheduled e pelo state=stopped / start
import boto3
ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
    filter = [
          {
              'Name' : 'tag:Type',
              'Values':['Scheduled']
          },
            {
              'Name': 'instance-state-name',
              'Values': ['stopped']
              
            }
      ]
    instances = ec2.instances.filter(Filters=filter)
    for instance in instances:
        instance.start(),
        for tag in instance.tags:
            print(f'InstanceId={instance.id} - Tag: {tag["Key"]}={tag["Value"]}')
      
    return 'Success'

```

https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html  
https://pt.thetimenow.com/brazil/s%c3%a3o_paulo  
https://www.worldtimebuddy.com/  


### Criar schedule para ligar as 06:00 AM (GMT-3)

>## No Cloudwatch, colocar GMT-3
### CloudWatch - Events - Create Role - Schedule (GMT): 0 9 ? * MON-FRI *
### Add trarget - lambdaFunction - schedule_ec2_instances