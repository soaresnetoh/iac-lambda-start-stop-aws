# list all instances whit tag:Type = "Scheduled"
aws ec2 describe-instances --region us-east-1 \
    --filters Name=tag:Type,Values=Scheduled \
    --query 'Reservations[*].Instances[*].{Instance:InstanceId,AZ:Placement.AvailabilityZone,Name:Tags[?Key==`Name`]|[0].Value}' \
    --output table


# List only InstanceId
aws ec2 describe-instances --region us-east-1 \
    --filters Name=tag:Type,Values=Scheduled \
    --query 'Reservations[*].Instances[*].InstanceId' \
    --output text



aws ec2 describe-instances --region us-east-2 \
    --filters Name=tag:Devops,Values=test \
    --query 'Reservations[*].Instances[*].{Instance:InstanceId,AZ:Placement.AvailabilityZone,Name:Tags[?Key==`Name`]|[0].Value}' \
    --output table    