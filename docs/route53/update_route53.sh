#!/bin/bash

===============================================================

export TARGET_ENVIRONMENT=uat 
export BASE_ENVIRONMENT_DNS=abcd-External-9982627718-1916763929.us-west-1.elb.amazonaws.com

# Creates route 53 records based on env name
aws route53 change-resource-record-sets --hosted-zone-id 1234567890ABC --change-batch file://<(cat << EOF
{
  "Comment": "Testing creating a record set",
  "Changes": [
    {
      "Action": "CREATE",
      "ResourceRecordSet": {
        "Name": "$(TARGET_ENVIRONMENT).company.com",
        "Type": "CNAME",
        "TTL": 120,
        "ResourceRecords": [
          {
            "Value": "$(BASE_ENVIRONMENT_DNS)"
          }
        ]
      }
    }
  ]
}
EOF
)

===============================================================

aws route53 change-resource-record-sets --hosted-zone-id Z04820891PWH5E76X2H3W --change-batch file://<(cat << EOF
{
    "Comment": "Update record to reflect new IP address for a system ",
    "Changes": [
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": "blog.hsninformatica.com.br",
                "Type": "A",
                "TTL": 10,
                "ResourceRecords": [
                    {
                        "Value": "10.79.252.13"
                    }
                ]
            }
        }
    ]
}
EOF
)

===============================================================

# Update/Create RecordSet in file change-resource-record-sets.json
aws route53 change-resource-record-sets --hosted-zone-id Z04820891PWH5E76X2H3W --change-batch file://change-resource-record-sets.json

===============================================================

# Listar 
aws route53 list-resource-record-sets --hosted-zone-id Z04820891PWH5E76X2H3W \
--query "ResourceRecordSets[?Type == 'A']"

===============================================================
