variable "region" {
  description = "AWS Region"
  default     = "us-east-1"
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t3.micro"
}

variable "profile" {
  description = "Profile"
  default     = "hernanitotal"
}

variable "keypair" {
  description = "Keypair"
  default     = "hsn-key"
}

variable "key_path" {
  description = "Public key path"
  default     = "/home/usuario/.ssh/id_rsa.pub"
}
