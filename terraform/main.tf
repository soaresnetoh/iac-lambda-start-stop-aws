terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region  = var.region
  profile = var.profile
}


// Usado quando quer criar uma key
resource "aws_key_pair" "myawskeypair" {
  key_name   = "hsn-key"
  public_key = file("${var.key_path}")
}
