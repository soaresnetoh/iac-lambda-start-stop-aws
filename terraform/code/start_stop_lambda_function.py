# Function Name: schedule_ec2_instances
# Filtra instancias pelas tag de Name:Type e Values:Scheduled e pelo state=stopped / start
import boto3
ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
    filter = [
          {
              'Name' : 'tag:Type',
              'Values':['Scheduled']
          },
            {
              'Name': 'instance-state-name',
              'Values': ['stopped']
              
            }
      ]
    instances = ec2.instances.filter(Filters=filter)
    for instance in instances:
        instance.start(),
        for tag in instance.tags:
            print(f'InstanceId={instance.id} - Tag: {tag["Key"]}={tag["Value"]}')
      
    return 'Success'