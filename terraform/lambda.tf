resource "aws_iam_role" "start_stop_policy-lambda_role" {
  name               = "start_stop_policy_Lambda_Function_Role"
  assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "lambda.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_policy" "start_stop_policy" {
  name        = "start_stop_policy"
  description = "A start/stop instance policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances",
                "ec2:StartInstances",
                "logs:*",
                "ec2:StopInstances"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
resource "aws_iam_policy_attachment" "start_stop_policy-attach" {
  name = "start_stop_policy-attachment"
  //   users      = [aws_iam_user.user.name]
  roles = [aws_iam_role.start_stop_policy-lambda_role.name]
  //   groups     = [aws_iam_group.group.name]
  policy_arn = aws_iam_policy.start_stop_policy.arn
}


data "archive_file" "python_lambda_package_start_stop" {
  type        = "zip"
  source_file = "${path.module}/code/start_stop_lambda_function.py"
  output_path = "start_stop_lambda_function.zip"
}


resource "aws_lambda_function" "lambda_function_start_stop" {
  function_name    = "start_stop_lambda_function"
  filename         = "start_stop_lambda_function.zip"
  source_code_hash = data.archive_file.python_lambda_package_start_stop.output_base64sha256
  role             = aws_iam_role.start_stop_policy-lambda_role.arn
  runtime          = "python3.6"
  handler          = "start_stop_lambda_function.lambda_handler"
  timeout          = 10
}



